### universis-students


## Installation
  Installation instructions can be found in the [installation file](INSTALL.md)

## Contributing

Developers can check out our [contributing guide](CONTRIBUTING.md).

We use Gitlab to track bugs and feature requests for the code. When submitting a bug please make sure you submit a comprehensive description and all relevant information.

## Our Technologies

* Angular

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md)

  * With Karma and Protractor



## Modules

The project consists of multiple modules.

#### MostModule (@themost/angular)

universis-students uses MOST Client Module for Angular 2.x-6.x in order to communicate with Universis API Server.

```
this.context.model("students/me/registrations").asQueryable()
      .orderBy('registrationYear desc')
      .thenBy('registrationPeriod desc')
      .getItems().then((res)=> {
          this.registrations = res.value;
});
```

[@themost/client documentation](https://github.com/themost-framework/themost-client/tree/master/modules/%40themost/angular)      


#### SharedModule (app/shared)

SharedModule contains a set of services which can be used by the other application modules.

##### ConfigurationService

Application configuration is loaded on application start-up. Configuration data are static JSON files in `src/app/assets/config` directory.

    {
        "settings": {
            "app": {

            },
            "remote": {
                "server":"http://api.universis.io/api/"
            },
            "auth": {
                "authorizeURL":"http://users.universis.io/authorize",
                "logoutURL":"http://users.universis.io/logout?continue=https://localhost:7001/#/auth/login",
                "userProfileURL":"http://users.universis.io/me",
                "oauth2": {
                    "tokenURL": "http://users.universis.io/tokeninfo",
                    "clientID": "2658375048506108",
                    "callbackURL": "http://localhost:7001/#/auth/callback",
                    "scope": [
                        "students"
                    ]
                }
            },
            "localization": {
                "cultures": [ "en","el" ],
                "default": "el"
            }
        }
    }

Note: app.json is the default configuration file.
- In **development** mode the configuration service will try to load app.development configuration file.
- In **production** mode the configuration service will try to load app.production.json.

The configuration service initializes also application localization by loading Angular locales and application i18n files. These files are stored in `src/assets/i18n` directory.

    i18n
        el.json
        en.json

A i18n localization file is typically a collection of key-value pairs of translation data.

    {
        "Home": "Home",
        "Dashboard": "Dashboard",
        "Overview":"Overview",
        "Courses":"Courses",
        "TotalByCourseType":"total by course type",
        "Results":"Results",
        "CoursesResults":"courses results",
        "SuccessedCourses": "Successed",
        "FailedCourses": "Failed",
        "AcademicPeriod": "Academic period",
        "Average": "Average",
        "AveragePerPeriod":"Average per period",
        "AveragePerPeriodInfo": "average grade per period",
        "Logout": "Logout",
        "Exams": "Exams",
        "Activity": "Activity",
        "RegistrationsTitle": "Registrations"
    }

It may also contain one or more groups of key-value pairs:

    {
        "Registrations": {
            "Title": "Registrations",
            "AcademicYear":"Academic Year",
            "Period": "Period",
            "Status": "Status",
            "Date": "Date",
            "DateModified": "Modification Date",
            "Details": "Details"
        }
    }

Each one of application module may also contain a set of translation data. These files are stored in module directory (e.g. `/src/app/registration/i18n`)

    src
        + app
            + registrations
                + i18n
                    registrations.el.json
                    registrations.en.json

These localization data are loaded during module start-up.

`src/app/registrations/registrations.module.ts`

```
export class RegistrationsModule {
    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/registrations.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
```

#### AuthModule (app/auth)

AuthModule contains a set of components and services for user authentication and authorization.

##### AuthenticationService

universis-students uses the OAuth2 Implicit Authorization Flow. The configuration settings of OAuth2 server are stored in application configuration:

```
"auth": {
    "authorizeURL":"https://users.universis.io/authorize",
    "logoutURL":"https://users.universis.io/logout?continue=http://localhost:7001/#/auth/login",
    "userProfileURL":"https://users.universis.io/me",
    "oauth2": {
        "tokenURL": "https://users.universis.io/tokeninfo",
        "clientID": "2658375048506108",
        "callbackURL": "http://localhost:7001/#/auth/callback",
        "scope": [
            "students"
        ]
    }
}
```

##### AuthGuard

AuthGuard service is a typical Angular authorization guard. Application uses this guard to allow or deny access to specific application routes.

`src/app/app.routing.ts`

```
{
    path: '',
    component: FullLayoutComponent,
    canActivate: [
        AuthGuard
    ],
    data: {
        title: 'Home'
    },
    children: [
        {
            path: 'dashboard',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
        },
        {
            path: 'registrations',
            loadChildren: './registrations/registrations.module#RegistrationsModule'
        }
    ]
}
```

AuthGuard uses a configuration file to store these settings `src/app/auth/guards/auth.guard.locations.json`

```
[
    {
        "privilege":"Location",
        "target": {
            "url":"^/auth/"
        },
        "mask": 1
    },
    {
        "privilege":"Location",
        "target": {
            "url":"^/error"
        },
        "mask": 1
    },
    {
        "privilege":"Location",
        "account": {
            "name":"Students"
        },
        "target": {
            "url":"^/"
        },
        "mask": 1
    }
]
```

#### ErrorModule (app/error)

ErrorModule contains a set of components and services for error handling.

#### RegistrationsModule (app/error)

RegistrationsModule contains a set of components for displaying student registration data.
