import { Component } from '@angular/core';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { ProfileService } from "../../../profile/services/profile.service";
import {TranslateService} from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { Router } from '@angular/router';
import { LoadingService} from '../../../shared/services/loading.service';
import { ModalService } from '../../../shared/services/modal.service';


@Component({
    selector: 'registrations-courses',
    templateUrl: 'registrations-courses.component.html',
    styleUrls: ['registrations-courses.component.scss']
})

export class RegistrationCoursesComponent {
    //student's department
    private department: any;

    //current registration. Contains courses already registered by the student 
    private currentRegistration: any;

    //courses available for the student to register
    public availableCourses: any;

    //courses available for the student to register
    private allAvailableCourses: any;

    //code for the shared messagebox
    private code: string;

    //Registration Effective Status
    private effectiveStatus: any;

    private totalUnits: number = 0;
    private totalEcts: number = 0;

    //Groups of semesters
    private semesters: any;
    //Groups of course types
    private courseTypeGroups: any;

    //this attribute is equal to either semesters or courseTypeGroups
    //depending on the grouping choice
    private groups: any;

    //boolean: true if group by semester, false if group by type
    public semesterGroup: boolean = true;

    private canRegister: boolean = false;

    private registrationPeriodStart: Date;
    private registrationPeriodEnd: Date;
    public start: string;
    public end: string;

    private openRegistrationPeriod: boolean = false;
    private periodNotOpened: boolean;
    private periodPassed: boolean;

    //param to pass to the translation of the period
    private currentPeriod;

    private lastRegistration: any;
    private choosenSemester: any;
    private footerIsOpen: boolean = false ;
    private loading: boolean=true;


    constructor(private profileService: ProfileService,
                private currRegService: CurrentRegistrationService,
                private translate: TranslateService,
                private _context: AngularDataContext,
                private router: Router,
                private loadingService: LoadingService,
                private modalService: ModalService) {
    }

    //This function will initialize department, availableClasses and currentRegistration
    ngOnInit() {
        this.loadingService.showLoading();  // show loading
        this.profileService.getStudent().then((student) => {
            //get student department
            this.department = student.department;
            this.registrationPeriodStart = this.department.registrationPeriodStart;
            this.registrationPeriodEnd = this.department.registrationPeriodEnd;

            if(this.registrationPeriodStart && this.registrationPeriodEnd){
                this.start = (this.registrationPeriodStart.getDate().toString() + '/' + 
                + (this.registrationPeriodStart.getMonth()+1).toString() + '/' +
                + this.registrationPeriodStart.getFullYear());
    
                this.end = (this.registrationPeriodEnd.getDate().toString() + '/' + 
                + (this.registrationPeriodEnd.getMonth()+1).toString() + '/' +
                + this.registrationPeriodEnd.getFullYear());
            }else{
                this.start = this.end = null;
            }

            this.currentPeriod = {
                period:  this.department.currentPeriod.name,
                year: this.department.currentYear.name
            }

            // get the available classes for this registration session
            this.currRegService.getAvailableClasses().then(res => {
                this.allAvailableCourses = this.availableCourses = res.value;

                // get the latest registration
                this.currRegService.getCurrentRegistration(true).then(currReg => {
                    this.currentRegistration = currReg;

                    // If there are courses in the registration
                    if(this.currentRegistration.classes){
                        // add an extra attribute: isRegisteredCourse to all the available
                        // classes in order to distinguise between the already registered
                        // ones, ie the ones contained in the currentRegistration
                        this.addRegistrationAttribute();

                        //i nitialize the ects displayed at bottom window
                        this.initializeEcts();
                    }

                    this.getGroupData("semester").then(semesters => {
                        // save the semesters for the left semesters menu
                        this.semesters = semesters.value;

                        // initialize the grouping by semester
                        this.groupBySemester();

                        // Check wether we are between registration period deadlines
                        this.checkRegistrationPeriodDeadlines();

                        // Check the registration period effective status
                        this.checkRegistrationStatus();
                    })
                    this.loading = false; // Data is loaded
                    this.loadingService.hideLoading(); // hide loading
                }).catch(err => {
                    console.log('Current Registration does not exist')

                    // Check the registration period effective status
                    this.checkRegistrationStatus();

                    this.loading = false; // Data is loaded
                    this.loadingService.hideLoading(); // hide loading
                })
            })
        });
    }

    //Function to check the Registration status and show appropriate messages at the screen
    checkRegistrationStatus() {
        //Make an API call to get the Effective Status of the current Registration
        this.currRegService.getCurrentRegistrationEffectiveStatus().then(effectiveStatus => {           
            this.effectiveStatus = effectiveStatus;
            //Set the code property of this component equal to the
            //code property of the returned status.
            //This allows us to create a messagebox 
            //at the HTML file(registrations-courses.component.html)
            //with the parameters of the messagebox(icon, message, etc...)
            //taken from the 'registrations.el.json' file using 
            //this code as a key
            this.code = this.effectiveStatus.code;

            if(this.effectiveStatus.status == "open"){
                this.openRegistrationPeriod = true;

                if(this.effectiveStatus.code != "OPEN_NO_TRANSACTION"
                && this.effectiveStatus.code != "OPEN_NO_REGISTRATION"
                && this.effectiveStatus.code != "SELECT_SPECIALTY"){
                    //The first time this component is loaded it will redirect to checkout
                    //From checkout if we hit 'edit' we must come back here, so we 
                    //add a variable stay the first time we come and read it the next times
                    //so we now we must stay in this page.
                    //NOTE: A better routing design is needed here!
                    if(sessionStorage["edit"] == "TRUE"){
                        sessionStorage["edit"]="FALSE";
                        this.canRegister = true;
                    }else{
                        this.loading = false; // Data is loaded
                        this.loadingService.hideLoading(); // hide loading
                        this.router.navigate(['/registrations/courses/checkout']);
                    }
                }
            }else if(this.effectiveStatus.status == "closed"){
                if(this.effectiveStatus.code == "CLOSED_NO_TRANSACTION" ||
                    this.effectiveStatus.code == "CLOSED_NO_REGISTRATION" || 
                    this.effectiveStatus.code == "CLOSED_PERIOD_NO_TRANSACTION"){
                    
                    //check department flag
                    if(this.department.isRegistrationPeriod){
                        //Then check component flag to see if current date
                        //is between registration period deadlines and show the
                        //appropriate message('Not opened yet' vs 'Deadline passed')
                        if(this.periodNotOpened){
                            this.code = "NOT_OPENED_YET";
                        }else if(this.periodPassed){
                            this.code = "PASSED";
                        }else{
                            this.code = "CLOSED_GENERAL";
                        }
                    }else{
                        this.code = "CLOSED_GENERAL"
                    }
                }else if(this.effectiveStatus.code == "CLOSED_REGISTRATION_PERIOD"){
                    this.code = "CLOSED_GENERAL";
                }
            }
        });
    }

    // Function triggered when user clicks on one of the available courses
    registerForCourse(course){
        // call the service method to register for course
        this.currRegService.registerForCourse(course).then(res => {
            // change the attribute of the course to "true"
            // Note: In JS arguements are passed by reference so 
            // a change here will change the original course too
            course.isRegisteredCourse = true;

            // update the added courses array to show the new added course
            // at the bottom window. The service function called above will
            // save the new course to session storage so we can take it from there
            this.currentRegistration = JSON.parse(sessionStorage["RegistrationLatest"]);

            // Add the course units and ects to the total sum
            this.totalUnits += course.units;
            this.totalEcts += course.ects;
        });
    }

    removeCourse(course){
        this.currRegService.removeCourse(course).then(res => {
            this.totalUnits -= course.units;
            this.totalEcts -= course.ects;
            course.isRegisteredCourse = false;

            //update current registration
            if(this.currentRegistration.classes.length > 0){
                this.currentRegistration.classes = (JSON.parse(sessionStorage["RegistrationLatest"])).classes;
            }
        });
    }

    //check all the available courses to see which of these are already 
    //registered and set a new attribute "isRegisteredCourse" accordingly
    addRegistrationAttribute(){
        //for every available course
        for(let i=0; i<this.availableCourses.length; ++i){
            this.allAvailableCourses[i]["isRegisteredCourse"] = false;

            //check at current registration(check by course class)
            //We check both courseClass and courseClass.id
            for(let j=0; j<this.currentRegistration.classes.length; ++j){
                if(this.allAvailableCourses[i].courseClass == 
                    this.currentRegistration.classes[j].courseClass
                    || this.allAvailableCourses[i].courseClass ==
                        this.currentRegistration.classes[j].courseClass.id
                    ){
                   this.allAvailableCourses[i]["isRegisteredCourse"] = true;
                   break;
                }
            }
        }
    }

    checkRegistrationPeriodDeadlines(): void {
        var today = new Date();
        if(today < this.registrationPeriodStart){
            this.periodNotOpened = true;
        }else if(today > this.registrationPeriodEnd){
            this.periodPassed = true;
        }else{
            this.periodNotOpened = this.periodPassed = false;
        }
    }

    enableCoursesRegistration(){
        if(this.effectiveStatus.status == "open"){
            if(this.effectiveStatus.code == "SELECT_SPECIALTY"){
                this.translate.get(['Registrations']).subscribe(translations => {
                    this.modalService.openDialog(translations.Registrations.SelectSpecialtyTitle, translations.Registrations.SelectSpecialtyMessage).then(()=> {
                        // do nothing
                    });
                });
            }else{
                this.canRegister = true;
            }
        }
    }

    filterBySemester(semester) {                                            
        this.choosenSemester = semester;

        this.availableCourses = this.allAvailableCourses.filter(x => {
            return x.semester.id === this.choosenSemester;
        });

        //notation used by other groups
        this.groups =[{courses: this.availableCourses}];
    }

    changefooterIsOpen(parametr){
        this.footerIsOpen = parametr;
    }

    /**
     *
     * @param {string} groupByAttribute
     */
    getGroupData(groupByAttribute) {
        return this._context.model('students/me/availableClasses')
            .select(groupByAttribute, `count(${groupByAttribute}) as count`)
            .groupBy(groupByAttribute)
            .orderBy(groupByAttribute)
            .getItems().then( groups => {
                    return groups;
            });
    }

    groupByCourseType(){
        this.getGroupData("courseType").then(groups => {
            this.courseTypeGroups = groups.value;
            
            this.courseTypeGroups.forEach(x => {
                x["courses"] = this.allAvailableCourses.filter(course => {
                    return x.courseType.id === course.courseType.id;
                })
            });
            this.groups = this.courseTypeGroups;
            this.semesterGroup = false;
        });
    }

    groupBySemester(){
        this.semesters.forEach(x => {
            x["courses"] = this.allAvailableCourses.filter(course => {
                return x.semester.id === course.semester.id;
            });
        });
        this.groups = this.semesters;
        this.semesterGroup = true;
    }

    onChangeGroup(selected: any){
        if(selected=="semester"){
            this.groupBySemester();
        }else{
            this.groupByCourseType();
        }
    }

    removeSemesterFiltering(){
        this.groups = this.semesters;
        this.choosenSemester = null;
    }

    toggleBlock(item: any) {
        item.isHidden = !item.isHidden;
    }

    //initialize total units and ects from added courses
    initializeEcts(){
        for(let i=0; i<this.currentRegistration.classes.length; ++i){
            this.totalUnits += this.currentRegistration.classes[i].units;
            this.totalEcts += this.currentRegistration.classes[i].ects;
        }
    }
}
