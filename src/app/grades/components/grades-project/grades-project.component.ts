import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../services/grades.service';

@Component({
  selector: 'app-grades-project',
  templateUrl: './grades-project.component.html',
  styleUrls: ['./grades-project.component.scss']
})
export class GradesProjectComponent implements OnInit {

  public thesisInfo: any;

  constructor(private gradesService: GradesService) {

    this.gradesService.getThesisInfo().then((res) => {
      this.thesisInfo = res.value;
    });
  }

  ngOnInit() {
  }

}
