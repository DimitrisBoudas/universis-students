import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradesStatboxComponent } from './grades-statbox.component';

describe('GradesStatboxComponent', () => {
  let component: GradesStatboxComponent;
  let fixture: ComponentFixture<GradesStatboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradesStatboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesStatboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
