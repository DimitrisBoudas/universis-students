import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import { GradesRecentComponent } from './components/grades-recent/grades-recent.component';
import { GradesAllComponent } from './components/grades-all/grades-all.component';
import {GradesHomeComponent} from "./components/grades-home/grades-home.component";
import {GradesProjectComponent} from './components/grades-project/grades-project.component';

const routes: Routes = [

    {
        path: '',
        component: GradesHomeComponent,
        canActivate: [
            AuthGuard
        ],
        children: [
            {
                path: '',
                redirectTo: 'recent'
            },
            {
                path: 'all',
                component: GradesAllComponent
            },
            {
                path: 'recent',
                component: GradesRecentComponent
            },
          {
            path: 'project',
            component: GradesProjectComponent
          }

        ]
    }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradesRoutingModule { }
