import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ChartsModule} from 'ng2-charts/ng2-charts';

import { DashboardComponent} from './dashboard.component';
import { DashboardRoutingModule} from './dashboard-routing.module';
import { TranslateModule, TranslateService} from '@ngx-translate/core';
import { MostModule} from "@themost/angular/module";
import { CommonModule} from "@angular/common";
import { GradesModule} from '../grades/grades.module';
import { ProgressBarDegreeComponent } from './components/progress-bar-degree/progress-bar-degree.component';
import { ProgressBarSemesterComponent } from './components/progress-bar-semester/progress-bar-semester.component';
import { StudentRecentCoursesComponent } from './components/student-recent-courses/student-recent-courses.component';
import { StudentRecentGradesComponent } from './components/student-recent-grades/student-recent-grades.component';
import {environment} from '../../environments/environment';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ChartsModule,
        TranslateModule,
        MostModule,
        GradesModule,
        MostModule,
      TooltipModule.forRoot()
    ],
    declarations: [DashboardComponent,
        ProgressBarDegreeComponent,
        ProgressBarSemesterComponent,
        StudentRecentCoursesComponent,
        StudentRecentGradesComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DashboardModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/dashboard.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
