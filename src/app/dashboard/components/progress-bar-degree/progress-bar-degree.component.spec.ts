import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressBarDegreeComponent } from './progress-bar-degree.component';

describe('ProgressBarDegreeComponent', () => {
  let component: ProgressBarDegreeComponent;
  let fixture: ComponentFixture<ProgressBarDegreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressBarDegreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarDegreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
