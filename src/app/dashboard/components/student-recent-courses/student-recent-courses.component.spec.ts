import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentRecentCoursesComponent } from './student-recent-courses.component';

describe('StudentRecentCoursesComponent', () => {
  let component: StudentRecentCoursesComponent;
  let fixture: ComponentFixture<StudentRecentCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentRecentCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentRecentCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
