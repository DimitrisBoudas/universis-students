import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "../../services/authentication.service";


@Component({
  selector: 'app-logout',
  template: `<div></div>`,
  encapsulation: ViewEncapsulation.None,
})

export class LogoutComponent implements OnInit {

  constructor(private _router: Router,
    private _authService: AuthenticationService) {
  }

  ngOnInit(): void {
    // reset login status
    this._authService.logout().subscribe((logout) => {
      //
    });
  }
}
