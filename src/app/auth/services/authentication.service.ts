import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , from, of } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { AngularDataContext } from '@themost/angular/client';
import { ConfigurationService } from '../../shared/services/configuration.service';

export interface AuthCallbackResponse {
  access_token: string;
  expires: string;
  refresh_token: string;
  token_type: string;
  scope: string;
}

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient,
    private _config: ConfigurationService,
    private _context: AngularDataContext) {
  }


  authorize() {
    const settings = this._config.settings.auth;
    window.location.href = `${settings.authorizeURL}?response_type=token&client_id=${settings.oauth2.clientID}&scope=${settings.oauth2.scope}`;
  }


  /**
   * @param access_token
   * @returns {Observable<Observable<void>>}
   */
  callback(access_token) {
    const settings = this._config.settings.auth;
    const body = new HttpParams().set('access_token', access_token);
      const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post(settings.oauth2.tokenURL, body.toString(), {
      headers: headers
    }).flatMap(token => {
      this._context.setBearerAuthorization(access_token);
      return from(this._context.model('users/me').asQueryable().expand('groups').getItem()).map((res: any) => {
          if (res) {
              if (res.name === 'anonymous') {
                  throw new Error('Unauthorized');
              }
              const user = res;
              // set token info
              user.token = token;
              user.token.access_token = access_token;
              // store item to session storage
              sessionStorage.setItem('currentUser', JSON.stringify(user));
              // return current user
              return user;
          } else {
              throw new Error('Unauthorized');
          }
      });
    });
  }


  logout() {
    if (typeof sessionStorage.getItem('currentUser') === 'string') {
      // get current user
      const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
      // get authentication settings
      const settings  = this._config.settings.auth;
      // clear session storage
      sessionStorage.clear();
      // redirect to logout uri
      window.location.href = settings.logoutURL;
      return of(true);
    }
    return of(false);
  }
}
