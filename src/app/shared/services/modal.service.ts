import { Injectable, EventEmitter} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DialogComponent } from '../modals/dialog.component';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class ModalService {
  private modalRef: BsModalRef;



  public choice: string;
  config = {
    ignoreBackdropClick: true,
    keyboard: false,
    initialState: null,
    class: 'modal-content-base'
  };

  constructor(private modalService: BsModalService, private toastr: ToastrService) { }

  // Create a modal with custom html passed
  openModal(template: any): BsModalRef {
    return this.modalRef = this.modalService.show(template, this.config);
  }

  // A simple confirmation dialog
  // It returns a boolean Promise (true == confirmed)
  openDialog(title: string, description: string, isAlert?: boolean): Promise<boolean> {
    let configInitialized = JSON.parse(JSON.stringify(this.config));
    configInitialized.initialState = { 'title': title, 'description': description, 'isAlert': isAlert};
    const answer = this.modalService.show(DialogComponent, configInitialized).content.choice as EventEmitter<boolean>;
    return new Promise<boolean> ((resolv) => {
      answer.subscribe(res => {
        resolv(res);
      });
    });
  }

  notifySuccess(title: string, description: string) {
    this.toastr.success(description, title);
  }

  notifyError(title: string, description: string) {
    this.toastr.error(description, title);
  }

  notifyInfo(title: string, description: string) {
    this.toastr.info(description, title);
  }

  notifyWarning(title: string, description: string) {
    this.toastr.warning(description, title);
  }
}
